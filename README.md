# Kuantaz Technical Test Project

This project implements a solution for the Kuantaz technical test, which involves querying several endpoints, processing benefits information, and generating a new endpoint with a summary of the filtered and sorted benefits.

## Assumptions

- **Authentication**: No authentication is required for accessing the endpoints.
- **Database Models**: No direct database interactions were assumed; the focus was on API data processing.
- **External Data Consistency**: Assumed that the data received from endpoints is consistent and reliable.

## Requirements

- PHP 8.2 or higher
- Laravel 11.x
- Composer

## Installation

1. Clone the repository:

   ```
   git clone https://gitlab.com/astersnake/test-kuantaz.git
   ```

2. Navigate to the project directory:

   ```
   cd test-kuantaz
   ```

3. Install dependencies using Composer:

   ```
   composer install
   ```

4. Configure the `.env` file with access credentials for the endpoints.

## Usage

The project provides a new endpoint that includes the following information:

1. Benefits sorted by year.
2. Total amount per year.
3. Number of benefits per year.
4. Filter benefits that meet maximum and minimum amounts.
5. Each benefit should include its detail.
6. Benefits should be ordered from most recent to oldest year.

To access the endpoint, use the following URL:

```
http://127.0.0.1:8000/api/benefits
```

## Use of Laravel Collections

Laravel Collections have been used for handling and processing data obtained from the endpoints.

## Postman File

A Postman file has been created with requests to test the endpoints. You can import the `kuantaz.postman_collection.json` file in Postman to use it.

## Unit Testing

Unit tests have been implemented to verify the correct operation of the endpoints and the business logic. You can execute the tests using the following command:

```
php artisan test
```
## License

This project is licensed under the [MIT License](LICENSE).
