<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\BenefitResource;
use App\Services\Benefit\BenefitService;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BenefitController extends Controller
{
    public function __construct(private readonly BenefitService $benefitService)
    {
    }

    public function index(Request $request): JsonResponse
    {
        $benefits = $this->benefitService->general()->getBenefits();
        $filters = $this->benefitService->general()->getFilters();
        $records = $this->benefitService->general()->getRecords();

        $benefitsByYear = $benefits->groupBy(function ($benefit) {
            return Carbon::parse($benefit->date)->format('Y');
        })->map(function ($benefits, $year) use ($filters, $records) {
            $filteredBenefits = $benefits->filter(function ($benefit) use ($filters) {
                $filter = $filters->firstWhere('program_id', $benefit->program_id);
                return $filter && $benefit->amount >= $filter->min_value && $benefit->amount <= $filter->max_value;
            })->values();

            return [
                'year' => $year,
                'num' => $filteredBenefits->count(),
                'beneficios' => BenefitResource::collection($filteredBenefits->map(function ($benefit) use ($records) {
                    $record = $records->firstWhere('program_id', $benefit->program_id);
                    $benefit->ficha = $record;
                    return $benefit;
                })),
            ];
        })->sortByDesc('year')->values();

        return response()->json([
            'code' => 200,
            'success' => true,
            'data' => $benefitsByYear,
        ]);
    }
}
