<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BenefitResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id_programa' => $this->program_id,
            'monto' => $this->amount,
            'fecha_recepcion' => $this->reception_date,
            'fecha' => $this->date,
            'ficha' => new RecordResource($this->ficha),
        ];
    }
}
