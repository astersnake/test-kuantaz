<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class RecordResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'nombre' => $this->name,
            'id_programa' => $this->program_id,
            'url' => $this->url,
            'categoria' => $this->category,
            'descripcion' => $this->description,
        ];
    }
}
