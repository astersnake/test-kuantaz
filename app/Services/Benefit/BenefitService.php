<?php

namespace App\Services\Benefit;

use App\Services\Concerns\BuildBaseRequest;
use App\Services\Concerns\CanSendGetRequests;
use App\Services\Benefit\Resources\GeneralResource;

class BenefitService
{
    use BuildBaseRequest;
    use CanSendGetRequests;

    public function __construct()
    {
    }

    public function evaluate(mixed $response)
    {
    }

    public function general(): GeneralResource
    {
        return new GeneralResource(
            service: $this,
        );
    }
}
