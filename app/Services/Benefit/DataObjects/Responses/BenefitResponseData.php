<?php

namespace App\Services\Benefit\DataObjects\Responses;

use Spatie\LaravelData\Data;

class BenefitResponseData extends Data
{
    public function __construct(
        public int    $program_id,
        public int    $amount,
        public string $reception_date,
        public string $date,
    )
    {
    }
}
