<?php

namespace App\Services\Benefit\DataObjects\Responses;

use Spatie\LaravelData\Data;

class FilterResponseData extends Data
{
    public function __construct(
        public int $program_id,
        public string $name,
        public int $min_value,
        public int $max_value,
        public int $form_id,
    ) {
    }
}
