<?php

namespace App\Services\Benefit\DataObjects\Responses;

use Spatie\LaravelData\Data;

class RecordResponseData extends Data
{
    public function __construct(
        public int    $id,
        public string $name,
        public int    $program_id,
        public string $url,
        public string $category,
        public string $description,
    )
    {
    }
}

