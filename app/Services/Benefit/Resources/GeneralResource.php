<?php

namespace App\Services\Benefit\Resources;

use App\Services\Benefit\BenefitService;
use App\Services\Benefit\DataObjects\Responses\BenefitResponseData;
use App\Services\Benefit\DataObjects\Responses\FilterResponseData;
use App\Services\Benefit\DataObjects\Responses\RecordResponseData;
use Illuminate\Support\Collection;

class GeneralResource
{
    public function __construct(
        private BenefitService $service,
    )
    {
    }

    public function getBenefits(): Collection
    {
        $response = $this->service->get(
            request: $this->service->withBaseUrl(config('services.benefit.url')),
            url: config('services.benefit.benefit_endpoint'),
            query: []
        )->json();

        $benefits = collect($response['data'])->map(fn($item) => BenefitResponseData::from([
            'program_id' => $item['id_programa'],
            'amount' => $item['monto'],
            'reception_date' => $item['fecha_recepcion'],
            'date' => $item['fecha'],
        ]));

        return $benefits;
    }

    public function getFilters(): Collection
    {
        $response = $this->service->get(
            request: $this->service->withBaseUrl(config('services.benefit.url')),
            url: config('services.benefit.filter_endpoint'),
            query: []
        )->json();

        return collect($response['data'])->map(fn($item) => FilterResponseData::from([
            'program_id' => $item['id_programa'],
            'name' => $item['tramite'],
            'min_value' => $item['min'],
            'max_value' => $item['max'],
            'form_id' => $item['ficha_id'],
        ]));
    }

    public function getRecords(): Collection
    {
        $response = $this->service->get(
            request: $this->service->withBaseUrl(config('services.benefit.url')),
            url: config('services.benefit.record_endpoint'),
            query: []
        )->json();


        $records = collect($response['data'])->map(fn($item) => [
            'id' => $item['id'],
            'name' => $item['nombre'],
            'program_id' => $item['id_programa'],
            'url' => $item['url'],
            'category' => $item['categoria'],
            'description' => $item['descripcion'],
        ]);

        return $records->map(fn($item) => RecordResponseData::from($item));
    }
}
