<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key'    => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'slack'   => [
        'notifications' => [
            'bot_user_oauth_token' => env('SLACK_BOT_USER_OAUTH_TOKEN'),
            'channel'              => env('SLACK_BOT_USER_DEFAULT_CHANNEL'),
        ],
    ],
    "benefit" => [
        "url"     => env('BENEFIT_BASE_URL', "https://run.mocky.io/v3"),
        "benefit_endpoint" => env('BENEFIT_ENDPOINT', "399b4ce1-5f6e-4983-a9e8-e3fa39e1ea71"),
        "filter_endpoint"  => env('FILTER_ENDPOINT', "06b8dd68-7d6d-4857-85ff-b58e204acbf4"),
        "record_endpoint"  => env('RECORD_ENDPOINT', "c7a4777f-e383-4122-8a89-70f29a6830c0"),
    ],
];
