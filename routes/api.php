<?php

use App\Http\Controllers\API\BenefitController;
use Illuminate\Support\Facades\Route;


Route::get('/benefits', [BenefitController::class, 'index']);
