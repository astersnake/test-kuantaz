<?php

use App\Http\Controllers\API\BenefitController;
use App\Services\Benefit\BenefitService;
use App\Services\Benefit\DataObjects\Responses\BenefitResponseData;
use App\Services\Benefit\DataObjects\Responses\FilterResponseData;
use App\Services\Benefit\DataObjects\Responses\RecordResponseData;
use App\Services\Benefit\Resources\GeneralResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpFoundation\Response;

uses()->group('BenefitController');

beforeEach(function () {
    // Mock the BenefitService and GeneralResource
    $this->benefitService = $this->createMock(BenefitService::class);
    $this->generalResource = $this->createMock(GeneralResource::class);

    // Setup the return of general() to mock GeneralResource
    $this->benefitService->method('general')->willReturn($this->generalResource);

    $this->controller = new BenefitController($this->benefitService);
});

it('correctly processes and returns filtered benefits grouped by year', function () {
    // Arrange
    Config::set('services.benefit.url', 'http://example.com');
    Config::set('services.benefit.benefit_endpoint', '/benefits');
    Config::set('services.benefit.filter_endpoint', '/filters');
    Config::set('services.benefit.record_endpoint', '/records');

    // Creating instances of data objects
    $benefits = collect([
        new BenefitResponseData(program_id: 147, amount: 40656, reception_date: '09/11/2023', date: '2023-11-09'),
        new BenefitResponseData(program_id: 147, amount: 60000, reception_date: '10/10/2023', date: '2023-10-10'),
        new BenefitResponseData(program_id: 130, amount: 40656, reception_date: '08/09/2023', date: '2023-09-08'),
        new BenefitResponseData(program_id: 147, amount: 40656, reception_date: '10/07/2023', date: '2023-07-10')
    ]);
    $records = collect([
        new RecordResponseData(id: 922, name: 'Emprende', program_id: 147, url: 'emprende', category: 'trabajo', description: 'Fondos concursables para nuevos negocios'),
        new RecordResponseData(id: 2042, name: 'Subsidio Familiar (SUF)', program_id: 130, url: 'subsidio_familiar_suf', category: 'bonos', description: 'Beneficio económico mensual entregado a madres, padres o tutores que no cuentan con previsión social.')
    ]);
    $filters = collect([
        new FilterResponseData(program_id: 147, name: 'Emprende', min_value: 10000, max_value: 50000, form_id: 1),
        new FilterResponseData(program_id: 130, name: 'Subsidio Familiar (SUF)', min_value: 10000, max_value: 50000, form_id: 1)
    ]);


    // Mock methods in GeneralResource
    $this->generalResource->method('getBenefits')->willReturn($benefits);
    $this->generalResource->method('getRecords')->willReturn($records);
    $this->generalResource->method('getFilters')->willReturn($filters);

    $request = new Request();

    // Act
    $response = $this->controller->index($request);

    // Define the expected JSON structure
    $expectedJson = [
        'code' => 200,
        'success' => true,
        'data' => [
            [
                'year' => 2023,
                'num' => 3,
                'beneficios' => [
                    [
                        'id_programa' => 147,
                        'monto' => 40656,
                        'fecha_recepcion' => '09/11/2023',
                        'fecha' => '2023-11-09',
                        'ficha' => [
                            'id' => 922,
                            'nombre' => 'Emprende',
                            'id_programa' => 147,
                            'url' => 'emprende',
                            'categoria' => 'trabajo',
                            'descripcion' => 'Fondos concursables para nuevos negocios'
                        ]
                    ],
                    [
                        'id_programa' => 130,
                        'monto' => 40656,
                        'fecha_recepcion' => '08/09/2023',
                        'fecha' => '2023-09-08',
                        'ficha' => [
                            'id' => 2042,
                            'nombre' => 'Subsidio Familiar (SUF)',
                            'id_programa' => 130,
                            'url' => 'subsidio_familiar_suf',
                            'categoria' => 'bonos',
                            'descripcion' => 'Beneficio económico mensual entregado a madres, padres o tutores que no cuentan con previsión social.'
                        ]
                    ],
                    [
                        'id_programa' => 147,
                        'monto' => 40656,
                        'fecha_recepcion' => '10/07/2023',
                        'fecha' => '2023-07-10',
                        'ficha' => [
                            'id' => 922,
                            'nombre' => 'Emprende',
                            'id_programa' => 147,
                            'url' => 'emprende',
                            'categoria' => 'trabajo',
                            'descripcion' => 'Fondos concursables para nuevos negocios'
                        ]
                    ]
                ]
            ]
        ]
    ];

    // Convert the expected array to JSON and assert equality
    expect($response->getContent())->json()->toEqual($expectedJson);
});
