<?php

use App\Services\Benefit\BenefitService;
use App\Services\Benefit\DataObjects\Responses\BenefitResponseData;
use App\Services\Benefit\DataObjects\Responses\FilterResponseData;
use App\Services\Benefit\DataObjects\Responses\RecordResponseData;
use App\Services\Benefit\Resources\GeneralResource;
use Illuminate\Support\Facades\Http;

beforeEach(function () {
    $this->resource = new GeneralResource(new BenefitService());
});

it('gets benefits', function () {
    Http::fake([
        config('services.benefit.url') . '/*' => Http::response([
            'code' => 200,
            'success' => true,
            'data' => [
                [
                    'id_programa' => 1,
                    'monto' => 1000,
                    'fecha_recepcion' => '2023-06-01',
                    'fecha' => '2023-06-01',
                ],
            ],
        ]),
    ]);

    $benefits = $this->resource->getBenefits();

    expect($benefits)
        ->and($benefits->first())->toBeInstanceOf(BenefitResponseData::class)
        ->and($benefits->first()->program_id)->toBe(1)
        ->and($benefits->first()->amount)->toBe(1000)
        ->and($benefits->first()->reception_date)->toBe('2023-06-01')
        ->and($benefits->first()->date)->toBe('2023-06-01');
});

it('gets filters', function () {
    Http::fake([
        config('services.benefit.url') . '/*' => Http::response([
            'code' => 200,
            'success' => true,
            'data' => [
                [
                    'id_programa' => 1,
                    'tramite' => 'Filter 1',
                    'min' => 0,
                    'max' => 1000,
                    'ficha_id' => 1,
                ],
            ],
        ]),
    ]);

    $filters = $this->resource->getFilters();

    expect($filters)
        ->and($filters->first())->toBeInstanceOf(FilterResponseData::class)
        ->and($filters->first()->id)->toBe(1)
        ->and($filters->first()->name)->toBe('Filter 1')
        ->and($filters->first()->min_value)->toBe(0)
        ->and($filters->first()->max_value)->toBe(1000)
        ->and($filters->first()->form_id)->toBe(1);
});

it('gets records', function () {
    Http::fake([
        config('services.benefit.url') . '/*' => Http::response([
            'code' => 200,
            'success' => true,
            'data' => [
                [
                    'id' => 1,
                    'nombre' => 'Record 1',
                    'id_programa' => 1,
                    'url' => 'https://example.com',
                    'categoria' => 'Category 1',
                    'descripcion' => 'Description 1',
                ],
            ],
        ]),
    ]);

    $records = $this->resource->getRecords();

    expect($records)
        ->and($records->first())->toBeInstanceOf(RecordResponseData::class)
        ->and($records->first()->id)->toBe(1)
        ->and($records->first()->name)->toBe('Record 1')
        ->and($records->first()->program_id)->toBe(1)
        ->and($records->first()->url)->toBe('https://example.com')
        ->and($records->first()->category)->toBe('Category 1')
        ->and($records->first()->description)->toBe('Description 1');
});
